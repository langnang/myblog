# CSS 之常见问题的解决办法

<details>
<summary>父元素跟随子元素移动</summary>

**原因**

一个盒子如果没有上补白(`padding-top`)和上边框(`border-top`)，那么这个盒子的上边距会和其内部文档流中的第一个子元素的上边距重叠。

父元素的第一个子元素的上边距 `margin-top` 如果碰不到有效的 `border` 或者 `padding`.就会不断一层一层的找自己父元素，祖先元素进行垂直外边距合并。只要给父元素（或祖先元素）设置个有效的 `border` 或者 `padding` 就可以有效的管制子元素的 `margin` 防止它越级

**解决办法**

给父元素添加 `padding` 或 `border` 或 `overflow:hidden`

</details>

<details>
<summary>padding 影响 div 实际宽高</summary>

**原因**

默认 padding 是会占用父元素的宽高，会撑开容器宽高

**解决办法**

- `box-sizing:box-sizing`
- `display:flex`
</details>

<details>
<summary>inline-block 元素不对齐</summary>

**原因**

行内元素和替换元素（如\<input>\<textarea>等）中存在一个叫做基线（baseline）的东西，他的位置位于文本框的底部，即文字的最底端；当块状行内元素无文本填充的时候，它的基线就会自动移至元素的最底端；同时，图片以及其他非替换元素的基线也为元素的最底端。

总结起来就是，行内元素和替换元素的基线位于文本框的底端，无文本的块状行内元素、图片和非替换元素的基线就是元素的最底端。横向对齐的参照物默认就是这个基线（baseline）

所以元素不对齐的原因是其基线的位置不同所造成的，元素都参照基线进行了对齐。看似没有对齐，其实上还是对齐的

**解决办法**

- `vertical-align: top;`
- `vertical-align: bottom;`
</details>

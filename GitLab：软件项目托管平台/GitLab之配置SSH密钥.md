# GitLab之配置SSH密钥

## 前言

- GitLab
- Git之配置SSH密钥

## 配置SSH密钥

### 1. 登录GitLab，点击右上角用户头像，打开Settings

![](https://langnang.github.io/MyImageHosting/2020/11/824611.png)

### 2. 点击左侧SSH Keys

![](https://langnang.github.io/MyImageHosting/2020/11/7513597.png)

### 3. 将生成的SSH密钥填入，点击Add key按钮

![](https://langnang.github.io/MyImageHosting/2020/11/8400992.png)

### 4. SSH密钥添加完成

![](https://langnang.github.io/MyImageHosting/2020/11/4010247.png)

![](https://langnang.github.io/MyImageHosting/2020/11/7830211.png)

至此，GitLab配置SSH密钥完成。

END
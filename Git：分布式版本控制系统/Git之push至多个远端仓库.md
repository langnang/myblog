# Git之push至多个远端仓库

> 保持多个远端仓库代码同步

## 前言

- Git之生成SSH密钥
- GitHub | Gitee |GitLab |Bitbucket之配置SSH密钥
- GitHub | Gitee |GitLab |Bitbucket之新建仓库

**意义**

- 不同的远端仓库的环境条件不同，在不同地域可能存在各种优劣势
- 充分利用不同的平台的优势，来提高代码的有效使用性
- 多处备份同步，提高安全性

## 创建远端仓库

> 仓库名称不同不会对push操作有影响

### GitHub

![](https://langnang.github.io/MyImageHosting/2020/11/1957809.png)

![](https://langnang.github.io/MyImageHosting/2020/11/8152352.png)

### Gitee

![](https://langnang.github.io/MyImageHosting/2020/11/2902689.png)

![](https://langnang.github.io/MyImageHosting/2020/11/2703666.png)

### GitLab

![](https://langnang.github.io/MyImageHosting/2020/11/4693545.png)

![](https://langnang.github.io/MyImageHosting/2020/11/2667108.png)

### Bitbucket

![](https://langnang.github.io/MyImageHosting/2020/11/9014456.png)

![](https://langnang.github.io/MyImageHosting/2020/11/596316.png)

## clone某一远端仓库至本地，并将其它远端仓库的地址添加进去

> 推荐使用SSH方式，因HTTPS方式在每次push操作时都需要登录验证，若是多个远端仓库，则需多次验证，操作麻烦

### 1. clone某一仓库至本地

```bash
$ git clone git@github.com:langnang/push-one-to-many.git
```

![](https://langnang.github.io/MyImageHosting/2020/11/6805223.png)

### 2. 查看仓库远程版本库信息

```bash
$ cd push-one-to-many
$ git remote -v
```

![](https://langnang.github.io/MyImageHosting/2020/11/4701980.png)

### 3. 将其它仓库地址添加进去

```bash
$ git remote set-url --add origin git@gitee.com:langnang/push-one-to-many.git
$ git remote set-url --add origin git@gitlab.com:langnang/push-one-to-many.git
$ git remote set-url --add origin git@bitbucket.org:langnang/push-one-to-many.git
$ git remote -v
```

![](https://langnang.github.io/MyImageHosting/2020/11/5604937.png)

另一种添加仓库地址的方式就是配置`.git/config`文件

```txt
[core]
	repositoryformatversion = 0
	filemode = false
	bare = false
	logallrefupdates = true
	symlinks = false
	ignorecase = true
[remote "origin"]
	url = git@github.com:langnang/push-one-to-many.git
	fetch = +refs/heads/*:refs/remotes/origin/*
	url = git@gitee.com:langnang/push-one-to-many.git
	url = git@gitlab.com:langnang/push-one-to-many.git
	url = git@bitbucket.org:langnang/push-one-to-many.git
[branch "master"]
	remote = origin
	merge = refs/heads/master
```

## push至远端仓库

### 1. 添加个文件至仓库，然后提交

如新建个`README.md`文件

```markdown
# push-one-to-many

> Git之push至多个远端仓库
```

然后提交

```bash
$ git add .
$ git commit -m "测试push至多个远端仓库"
```

![](https://langnang.github.io/MyImageHosting/2020/11/4363611.png)

### 2. Push至多个远端仓库

```bash
$ git push origin --all
```

![](https://langnang.github.io/MyImageHosting/2020/11/8486904.png)

4个测试远端仓库全部push成功。

## 查看远端仓库

### GitHub

![](https://langnang.github.io/MyImageHosting/2020/11/1924678.png)

### Gitee

![](https://langnang.github.io/MyImageHosting/2020/11/2529060.png)

### GitLab

![](https://langnang.github.io/MyImageHosting/2020/11/9635235.png)

### Bitbucket

![](https://langnang.github.io/MyImageHosting/2020/11/2785468.png)

至此，成功push至多个远端仓库。

END
# Git之push代码需要登录的解决方法
> 使用git提交文件到github、gitee等远端仓库时,每次都要登录，操作起来很麻烦。

**原因**：在clone 项目的时候，使用了 https方式，而不是ssh方式

默认clone 方式是：HTTPS

![](https://langnang.github.io/MyImageHosting/2020/11/593861.png)

切换到：SSH方式

![](https://langnang.github.io/MyImageHosting/2020/11/7889741.png)

但切换到SSH方式是，需要本地生成SSH密钥并添加到GitHub，可参照以下内容完成配置

- Git之生成SSH密钥

- GitHub之配置SSH密钥

至此，成功解决。

END
# Git 之生成 SSH 密钥

> 加密传输，无需验证登录

## 前言

- [Git 是什么？](https://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-Git-%E6%98%AF%E4%BB%80%E4%B9%88%EF%BC%9F)

### 意义

- https 方式在每次 pull 和 push 时需要输入用户名和密码来验证登录，操作繁琐且麻烦
- 使用 SSH 方式可无需验证登录，操作简便，一步到位

## 启动控制台并检查是否已生成密钥

- 在桌面空白处右键鼠标，选中`Git Bash Here`打开控制台
- 查看是否已经有了 ssh 密钥：`cd ~/.ssh`
  > 如果没有密钥则不会有此文件夹，有则备份删除
  >
  > ![](https://langnang.github.io/MyImageHosting/ScreenShot/2020/11/1538906.png)

## 生成 SSH 密钥

### 1. 配置 Git 的用户名和邮箱

```bash
$ git config --global user.name "langnang"
$ git config --global user.email "langnang.chen@outlook.com"
```

![](https://langnang.github.io/MyImageHosting/ScreenShot/2020/11/5831253.png)

### 2. 生成密钥

```bash
$ ssh-keygen -t rsa -C "langnang.chen@outlook.com"
```

连按三次 Enter，最后会生成两个文件：id_rsa 和 id_rsa.pub

![](https://langnang.github.io/MyImageHosting/ScreenShot/2020/11/9367947.png)

位于 C 盘用户目录下的.ssh 文件夹中

![](https://langnang.github.io/MyImageHosting/ScreenShot/2020/11/5523189.png)

### 3. 查看公钥

```bash
$ cat ~/.ssh/id_rsa.pub
```

![](https://langnang.github.io/MyImageHosting/ScreenShot/2020/11/9920390.png)

至此，密钥生成成功。

END

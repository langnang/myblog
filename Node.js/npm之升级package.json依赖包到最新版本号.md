# npm 之升级 package.json 依赖包到最新版本号

## 安装

```bash
npm install -g npm-check-updates
```

## 使用：

检查 package.json 中 dependencies 的最新版本：

```bash
ncu
```

更新 dependencies 到新版本：

```bash
ncu -u
```

更新全部到最新版本：

```bash
npm install
```

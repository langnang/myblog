# Awesome | Node.js

- chalk: Terminal string styling done right.
- commander: The complete solution for node.js command-line interfaces.
- download-git-repo: Download and extract a git repository (GitHub, GitLab, Bitbucket) from node.
- inquirer: A collection of common interactive command line user interfaces.
- ora: Elegant terminal spinner.

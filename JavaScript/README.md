# JavaScript

## Syntax and Basic Constructs <small>// 语法和基本结构</small>

- 介绍、历史与发展
- 数据类型（原始类型、值类型、引用类型） | 数据类型判断 | null 和 undefined 的区别
  - Array | 数组
  - Boolean | 布尔值
  - Number | 数值
  - Null | 空
  - String | 字符串
  - Object | 对象
  - Undefined | 未定义
  - Symbol | 独一无二的值
  - Function | 函数
- 基本语法
- [操作符](./JavaScript 操作符.md)
- [基本语句](./JavaScript 基本语句.md)
- 函数表达式
- [JavaScript 对象参考手册](./JavaScript 对象参考手册.md)

## DOM Manipulation <small>// DOM 操作</small>

- 提取 CSS 文件中所有类名：`document.styleSheets[index].cssRules`

## Fetch API/Ajax(XHR) <small>// 资源接口</small>

- fetch api
- ajax

## ES6+ and modular JavaScript <small>// ES6+ 和模块化</small>

- ES6/ES2015（Promise、Symbol、Generator、async、Class、Module）

## Understand <small>// 概念理解</small>

- this、call、apply、bind

- [闭包](https://github.com/langnang/MyBlog/issues/67)：指有权访问另一个函数作用域中的变量的函数。
- 执行上下文与执行上下文栈
- 继承、原型与原型链
- 原型与原型链
  - 原型：给其它对象提供共享属性的对象，简称为原型( prototype )。
  - 原型链：如果要访问对象中并不存在的一个属性，就会查找对象内部 prototype 关联的对象。在查找属性时会对它进行层层寻找遍历，这个关联关系实际上定义了一条原型链。
- 作用域（函数、块级、词法作用域）和作用域链
  - 函数作用域
  - 块级作用域
  - 链式作用域：JavaScript 中的函数运行在他们被定义的作用域里，而不是他们被执行的作用域里。
- 变量提升：函数及变量的声明都将被提升到函数的最顶部
- 自由变量
- == vs ====
- 立即执行函数, 模块化, 命名空间
- 垃圾回收机制
- 模块化
- 递归
- 算法
- 消息队列和事件循环
- 继承, 多态和代码复用
- 按位操作符, 类数组对象和类型化数组
- 如何实现浏览器内多个标签页之间的通信
- DOM 树和渲染过程
- null 和 undefined 的区别是什么
- 延迟加载的方式有哪些
- 同源策略
  - 协议、域名、端口相同
- eval 的功能：把对应的字符串解析成 JavaScript 代码并运行。但是应该避免使用 eval，使用它可能会造成程序不安全，影响性能因要一次解析成 JavaScript 语句，一次执行。
- this 对象：谁调用指向谁
- 事件代理（事件委托）
  - 利用事件冒泡的特性，将本应该注册在子元素上的处理事件注册在父元素上，这样点击子元素时发现其本身没有相应事件就到父元素上寻找作出相应。
- 事件冒泡
  - 事件从事件目标(target)开始，往上冒泡直到页面的最上一级标签。
- 事件捕获
  - 事件从最上一级标签开始往下查找，直到捕获到事件目标(target)。
- 定时器
  - setTimeout
  - setInterval
  - clearInterval
- 什么是未声明和未定义的变量
  - 未声明的变量出现中不存在且未声明的变量。如果程序尝试读取未声明变量的值，则会遇到运行时错误。
  - 未定义的变量是在程序中声明但尚未给出任何值的变量。如果程序尝试读取未定义变量的值，则返回未定义的值。
- 逻辑运算符
- 设计模式
- 纯函数, 函数副作用和状态变化
- 耗性能操作和时间复杂度
- JavaScript 引擎
- 二进制, 十六进制, 十进制, 科学记数法
- 偏函数, 柯里化, Compose 和 Pipe
- 循环结构
  - for, while, do...while, for in, for of
- delete 操作符
- delete 操作符用于删除对象中的某个属性，但是不能删除变量，函数等
- 在 JavaScript 中有哪些类型的弹出框
  - alert, confirm, prompt
- 构造函数
  - 构造函数是用来创建对象时初始化对象，与 new 一起试用，创建对象的语句中构造函数的名称必须与类名完全相同。
- 自执行函数
  - 自执行函数是指声明的一个匿名函数，可以立即调用整个匿名函数，一般用于框架，插件等场景，好处在于避免各种 JavaScript 库的冲突，隔离作用域，避免污染。
- 函数的三种定义方式
  - 1、 函数式声明
  - 2、 函数表达式（函数字面量）
  - 3、 函数构造法，参数必须加引号
  - 4、 对象直接量
  - 5、 原型继承
  - 6、 工厂模式
- DOM
  - DOM，文档对象模型（Document Object Model）。DOM 是 W3C（万维网联盟）的标准，DOM 定义了访问 HTML 和 XML 文档的标准。在 W3C 的标准中，DOM 是独于平台和语言的接口，它允许程序和脚本动态地访问和更新文档的内容、结构和样式。
  - 分三种：
  - 核心 DOM，针对任何结构化文档的标准模型
  - xml Dom，针对 xml 文档的标准模型
  - html Dom，针对 HTML 文档的标准模型
- cookie 和 session 的区别

  - cookie 数据存放在客户的浏览器上，session 数据存放在服务器上
  - cookie 不是很安全
  - session 会在一定时间内保持在服务器上，当访问多时，会影响服务器的性能。
  - 用户验证这种场合一般会用 session
  - session 可以放在 文件、数据库、或内存中都可以
  - Cookie 和 Session 都是会话技术
  - Cookie 有大小限制以及浏览器在存 cookie 的个数也有限制，Session 是没有大小限制和服务器的内存大小有关

- 代码整洁

- [闭包](./JavaScript/JavaScript之闭包.md)
- [递归](./JavaScript/JavaScript之递归.md)
- JavaScript 对象
  - [Array 对象](./JavaScript/JavaScript_对象之_Array.md)
  - [Boolean 对象](./JavaScript/JavaScript_对象之_Boolean.md)
  - [Date 对象](./JavaScript/JavaScript_对象之_Date.md)
  - [Events 对象](./JavaScript/JavaScript_对象之_Events.md)
  - [Global 对象](./JavaScript/JavaScript_对象之_Global.md)
  - [Math 对象](./JavaScript/JavaScript_对象之_Math.md)
  - [Number 对象](./JavaScript/JavaScript_对象之_Number.md)
  - [Object 对象](./JavaScript/JavaScript_对象之_Object.md)
  - [RegExp 对象](./JavaScript/JavaScript_对象之_RegExp.md)
  - [String 对象](./JavaScript/JavaScript_对象之_String.md)
- Browser 对象
  - [Browser History 对象](./JavaScript/JavaScript_Browser_对象之_History.md)
  - [Browser Location 对象](./JavaScript/JavaScript_Browser_对象之_Location.md)
  - [Browser Navigator 对象](./JavaScript/JavaScript_Browser_对象之_Navigator.md)
  - [Browser Screen 对象](./JavaScript/JavaScript_Browser_对象之_Screen.md)
  - [Browser Window 对象](./JavaScript/JavaScript_Browser_对象之_Window.md)
- HTML DOM 对象
  - [DOM Attribute 对象](./JavaScript/JavaScript_DOM_对象之_Attribute.md)
  - [DOM Document 对象](./JavaScript/JavaScript_DOM_对象之_Document.md)
  - [DOM Element 对象](./JavaScript/JavaScript_DOM_对象之_Element.md)
  - [DOM Event 对象](./JavaScript/JavaScript_DOM_对象之_Event.md)

## 参考手册

### JavaScript 对象

- Array
- Boolean
- Date
- Math
- Number
- String
- RegExp
- Global

### BOM 对象

- Window
- Navigator
- Screen
- History
- Location

### DOM 对象

- canvas
- form
- image
- table

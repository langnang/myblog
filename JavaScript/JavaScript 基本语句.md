# JavaScript 基本语句



## if <small>// 条件判断语句</small>

```js
if (condition) {
    // statement1
} else {
    // statement2
}
```



## do-while <small>// 后测试循环语句</small>
```js
do {
    // statement 
} while (expression);
```



## while <small>// 前测试循环语句</small>

```js
while (expression) {
    // statement
}
```



## for <small>// 前测试循环语句</small>

具有在执行循环之前初始化变量和定义循环后要执行的代码的能力。

```js
for (initialization; expression; post-loop-expression) {
    // statement
}
```



## for-in <small>// 精准的迭代语句</small>

可以用来枚举对象的属性

```js
for (property in expression) {
    // statement
}
```

## for-of

es6 新增

## label <small>// 在代码中添加标签</small>

```js
label: statement
```



## break & continue <small>// 在循环中精确地控制代码的执行</small>

break语句会立即退出循环，强制继续执行循环后面的语句。

而continue语句虽然也是立即退出循环，但退出循环后会从循环的顶部继续执行。

## with <small>// 将代码的作用域设置到一个特定的对象中</small>

```js
with (expression) statement;
```



## switch <small>// 流控制语句</small>

```js
switch (expression) {   
    case value: statement     
    	break;   
    case value: statement     
    	break;   
    case value: statement     
    	break;   
    case value: statement     
    	break;   
    default: statement 
}
```


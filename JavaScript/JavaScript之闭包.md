# JavaScript 之闭包

## 中英文全称

Closure

## 用途

能够访问另一个函数作用域的变量的函数。

## 核心概念或运作流程

在一个函数内部创建另一个函数，并使用 `return` 语句返回

## 代码思路

```js
function f() {
  var name = "Name";

  function alertName() {
    alert(name);
  }
  return alertName;
}

var myF = f();
myF();
```

## 优点

- 变量长期驻扎在内存中
- 避免全局变量的污染
- 私有成员的存在

## 缺点

- 比普通函数占用更多的内存
- 闭包只能取得包含函数中任何变量的最后一个值
- 引用的变量可能发生变化
- this 指向问题
- 内存泄漏问题

## 弥补

- 闭包不再使用时，及时释放

## 关联

- 函数
- 函数作用域
- 内存泄漏

## 参考链接

- [闭包 - JavaScript | MDN](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Closures)

# Gitee之配置SSH密钥

## 前言

- Gitee
- Git之配置SSH密钥

## 配置SSH密钥

### 1. 登录Gitee，点击右上角用户头像，打开设置

![](https://langnang.github.io/MyImageHosting/2020/11/4976840.png)

### 2. 点击左侧安全设置中的SSH公钥

![](https://langnang.github.io/MyImageHosting/2020/11/4646954.png)

### 3. 将生成的SSH密钥填入，点击确定按钮

> 需通过账号安全验证，即密码验证

![](https://langnang.github.io/MyImageHosting/2020/11/4168611.png)

### 4. SSH密钥添加完成

![](https://langnang.github.io/MyImageHosting/2020/11/1502583.png)

至此，Gitee配置SSH密钥完成。

END
# GitHub之配置SSH密钥



## 前言

- GitHub
- Git之配置SSH密钥



## 配置SSH密钥

### 1. 登录GitHub，点击右上角用户头像，打开Settings

![](https://langnang.github.io/MyImageHosting/2020/11/7041227.png)

### 2. 点击左侧SSH and GPG Keys

![](https://langnang.github.io/MyImageHosting/2020/11/2760000.png)

### 3. 点击右上角 New SSH Key

![](https://langnang.github.io/MyImageHosting/2020/11/9530287.png)

### 4. 将生成的SSH密钥填入，点击Add SSH Key


![](https://langnang.github.io/MyImageHosting/2020/11/2184888.png)

### 5. SSH密钥添加完成

![](https://langnang.github.io/MyImageHosting/2020/11/7442749.png)

至此，GitHub配置SSH密钥完成。

END
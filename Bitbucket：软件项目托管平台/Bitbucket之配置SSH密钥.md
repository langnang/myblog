# Bitbucket之配置SSH密钥

## 前言

- Bitbucket
- Git之配置SSH密钥

## 配置SSH密钥

### 1. 登录Bitbucket，点击左下角用户头像，打开Personal Settings

![](https://langnang.github.io/MyImageHosting/2020/11/6673642.png)

### 2. 点击左侧SECURITY中的SSH Keys

![](https://langnang.github.io/MyImageHosting/2020/11/7961799.png)

### 3. 点击Add Key按钮

![](https://langnang.github.io/MyImageHosting/2020/11/5328738.png)

### 4. 将生成的SSH密钥填入，点击Add key按钮

![](https://langnang.github.io/MyImageHosting/2020/11/9329160.png)

### 5. SSH密钥添加完成

![](https://langnang.github.io/MyImageHosting/2020/11/7639065.png)

至此，Bitbucket配置SSH密钥完成。

END
# MyBlog

![GitHub repo size](https://img.shields.io/github/repo-size/langnang/myblog) ![GitHub issues](https://img.shields.io/github/issues-raw/langnang/myblog)

- Algorithm
  - Sorting: 排序算法
    - [冒泡排序](./Algorithm/Sorting_Algorithm_之冒泡排序.md): 重复走访比较相邻元素，将最大/小元素移至末尾，并逐渐减少比较长度
    - [选择排序](./Algorithm/Sorting_Algorithm_之选择排序.md): 重复遍历未排序部分,将最小元素移至已排序末尾
    - [插入排序](./Algorithm/Sorting_Algorithm_之插入排序.md): 默认第一个元素为已排序,取未排序部分第一个元素,从后向前进行比较交换位置
    - [堆排序](./Algorithm/Sorting_Algorithm_之堆排序.md): 构建待排序序列成大项堆,交换堆顶元素与最后一个元素,调整新堆为大项堆
    - [归并排序](./Algorithm/Sorting_Algorithm_之归并排序.md): 递归分割序列,比较合并已排序序列
    - [快速排序](./Algorithm/Sorting_Algorithm_之快速排序.md): 根据基准值分割序列,左侧小于,右侧大于,递归执行后合并
    - [希尔排序](./Algorithm/Sorting_Algorithm_之希尔排序.md): 设置增量序列/因子,分割数组进行插入排序,直至增量因子为 1
    - [计数排序](./Algorithm/Sorting_Algorithm_之计数排序.md): 取最大值和最小值,统计元素出现次数,计数累加,反向填充输出
    - [基数排序](./Algorithm/Sorting_Algorithm_之基数排序.md): 取最大值及其位数,取元素的每个位组成技术序列,进行计数排序
    - [桶排序](./Algorithm/Sorting_Algorithm_之桶排序.md): 设置桶范围,遍历数据至对应的桶中,对非空桶进行排序后拼接
- ASP.NET Core: 跨平台框架
- BitBucket: 软件项目托管平台
  - [配置 SSH 密钥](./Bitbucket: 软件项目托管平台/Bitbucket 之配置 SSH 密钥.md)
- Charts: 图表
- CSS: 层叠样式表
  - [常见问题的解决办法](./CSS: 层叠样式表/CSS 之常见问题的解决办法.md)
- Data Structure: 数据结构
- Deno: A secure runtime for JavaScript and TypeScript.
- DNS: 域名系统
  - [DNS 之解析记录类型](./DNS: 域名系统/DNS\_之解析记录类型.md)
- ECMAScript: 标准化的脚本程序设计语言
- Function: function implementations
- Git: 分布式版本控制系统
  - [生成 SSH 密钥](./Git: 分布式版本控制系统/Git 之生成 SSH 密钥.md)
  - [push 代码需要登录的解决办法](./Git: 分布式版本控制系统/Git 之 push 代码需要登录的解决方法.md)
  - [push 至多个远端仓库](./Git: 分布式版本控制系统/Git 之 push 至多个远端仓库.md)
- Gitee: 软件项目托管平台
  - [配置 SSH 密钥](./Gitee: 软件项目托管平台/Gitee 之配置 SSH 密钥.md)
- GitHub: 软件项目托管平台
  - [配置 SSH 密钥](./GitHub: 软件项目托管平台/GitHub 之配置 SSH 密钥.md)
- GitLab: 软件项目托管平台
  - [配置 SSH 密钥](./GitLab: 软件项目托管平台/GitLab 之配置 SSH 密钥.md)
- Google
- Grafana: 开放式观测平台
- HTML: 超文本标记语言
- HTTP: 超文本传输协议
  - [协议状态码（Status Code）](./HTTP: 超文本传输协议/HTTP 之协议状态码.md)
  - [跨源资源共享 (CORS) ](./HTTP: 超文本传输协议/HTTP 之 CORS.md)
- Internet: 使用Internet协议组(TCP/IP)在网络和设备之间进行通信的互连计算机网络的全球系统。
  - HTTP
  - DNS
  - Domain Name
  - hosting
- [JavaScript](./JavaScript/README.md): 一种轻量级的、解释型的编程语言
- JetBrains: 软件开发工具
- LeetCode: 技术成长平台
- MariaDB: 数据库管理系统
- MySQL: 数据库管理系统
  - [版本查询](./MySQL: 数据库管理系统/MySQL 之版本查询.md)
- Node.js: a JavaScript runtime built on Chrome's V8 JavaScript engine.
  - npm: node 包管理器
  - [安装与配置](./Node.js/Node.jS之安装与环境配置.md)
  - [升级 package.json 依赖包到最新版本号](./Node.js/npm之升级package.json依赖包到最新版本号.md)
- PHP: 服务端脚本语言
- PostgreSQL: 数据库管理系统
- Python: 计算机程序设计语言
- Regular Expression: 正则表达式
- SQLite: 数据库管理系统
- SVG: 可缩放矢量图形
- Vue: 构建用户界面的渐进式框架
- Web: 分布式图形信息系统
  - [W3C 标准及规范](./Web: 分布式图形信息系统/Web*之\_W3C*标准及规范.md)
  - [浏览器内核](./Web: 分布式图形信息系统/Web\_之浏览器内核.md)
  - Web 安全
    - [XSS](./Web: 分布式图形信息系统/Web\_安全之\_XSS.md)
- XML: 可拓展标记语言

<details open>
    <summary>前端开发学习路线图</summary>
    <img src="https://roadmap.sh/roadmaps/frontend.png"/>
</details>

<details open>
    <summary>后端开发学习路线图</summary>
    <img src="https://roadmap.sh/roadmaps/backend.png"/>
</details>

